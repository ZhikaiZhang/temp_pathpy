from pypbd import TPGMM_Time
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import patches as patches
from math import sqrt, log
from dataclasses import dataclass
import seaborn as sns
import pandas as pd
import os
import random
import csv
import time


@dataclass
class Params:
    rule = 0  # SPECIFY WHICH TRIAL HERE - 0 = 4CM, 1 = ENTROPY
    shift = [-0.225, -0.05]
    goal_x = 0.3 + shift[0]
    goal_y = 0.4 + shift[1]
    demos_to_use = []
    increment = 0.03
    end_goal_radius = 0.015
    alpha = 0  # now that p-controller has been implemented, start_dis is essentially a useless metric
    beta = 0.3  # 1 distance from end point
    gamma = 0.4  # 100 collision
    delta = 0.0  # 1 drift
    epsilon = 0.3  # 100 out of bounds states
    pd_accuracy = 0.01
    k_p = 1
    directory_name = 'FINAL_joy_sample'#'Haomiao_test'
    all_files = []
    max_demos = 55
    reg = 1e-5
    max_gmm = 30
    demo_name = "temp_demo"


def obtain_files():  # iterates through directory to get all filenames
    for filename in os.listdir(settings.directory_name):
        f = os.path.join(settings.directory_name, filename)
        if filename[-4:] == ".npy":
            settings.all_files.append(filename)
    print(settings.all_files)


def retrieve_demos(demos_to_use):  # obtain demos
    for x in range(len(demos_to_use)):
        demo = np.expand_dims(np.load(settings.directory_name + '/' + demos_to_use[x]),
                              axis=2)  # examine demo 1
        if x == 0:
            data = demo
        else:
            data = np.concatenate((data, demo), axis=2)  # add new demo to existing demo array

    print("SHAPE", data.shape)
    print(data[1, :, :])
    print("number of demos are now: ", len(settings.demos_to_use))
    return data  # return full demo array


def plot_demos():  # visually plot demo entries
    for demo_name in settings.demos_to_use:
        data = np.expand_dims(np.load(settings.directory_name + '/' + demo_name), axis=2)  # examine demo 1
        plt.plot(data[:, 1, 0], data[:, 2, 0], color='blue', linewidth=0.7)  # plot blue line illustrating path
        # plt.scatter(demo[:,1,0], demo[:,2,0],zorder=100,s=5) #visualize the points in each trajectory
        print(demo_name, data[0, 1, 0], data[0, 2, 0])  # print out starting point


def get_frame_dtype(dim):  # get frame data type, from TPGMM_time.py
    return np.dtype([("A", "f8", (dim, dim)), ("b", "f8", (dim,))])  # A is transformation matrix, b is offset vector


def get_frames(data, dims):  # generate frames of observation
    frames = np.array([([[1, 0], [0, 1]], [data[0, 1, 0], data[0, 2, 0]]),  # start point
                       ([[1, 0], [0, 1]], [data[99, 1, 0], data[99, 2, 0]]),  # end point
                       #([[1, 0], [0, 1]], [-0.12 + settings.shift[0], 0.19 + settings.shift[1]])
                       #([[1, 0], [0, 1]], [data[99, 1, 0], data[99, 2, 0]])
                       ]  # obstacles
                      , dtype=get_frame_dtype(dims))
    frames = np.expand_dims(frames, axis=1)
    #print("frame 0 rotation matrix is now: ", frames[0, :]["A"], "frame 0 translation matrix is now: ", frames[0, :]["b"])
    #print("frame 1 rotation matrix is now: ", frames[1, :]["A"], "frame 1 translation matrix is now: ",
    #      frames[1, :]["b"])
    frames[1, :]["b"] = np.zeros(2) # edit frames


    for i in range(1, np.size(data, axis=2)):
        frame_demo = np.array([([[1, 0], [0, 1]], [data[0, 1, i], data[0, 2, i]]),  # start point
                               ([[1, 0], [0, 1]], [data[99, 1, i], data[99, 2, i]]),  # end point
                               #([[1, 0], [0, 1]], [-0.12 + settings.shift[0], 0.19 + settings.shift[1]])
                               #([[1, 0], [0, 1]], [data[99, 1, 0], data[99, 2, 0]])
                               ]  # obstacles
                              , dtype=get_frame_dtype(dims))

        frame_demo = np.expand_dims(frame_demo, axis=1)
        frame_demo[1, :]["b"] = np.zeros(2)  # edit frames
        frames = np.concatenate((frames, frame_demo), axis=1)
        i += 1
    print("frames is now: ", frames)
    return (frames)


def generate_trajectory(x_init, y_init, dims):  # generates a new trajectory starting from x_init and y_init
    # generate the new frame (only modifying start point observer)
    new_frame = np.array([([[1, 0], [0, 1]], [x_init, y_init]),  # start point
                          ([[1, 0], [0, 1]], [data[99, 1, 0], data[99, 2, 0]]),  # end point
                          #([[1, 0], [0, 1]], [-0.12 + settings.shift[0], 0.19 + settings.shift[1]])
                          #([[1, 0], [0, 1]], [data[99, 1, 0], data[99, 2, 0]])
                          ]  # obstacles
                         , dtype=get_frame_dtype(dims))

    new_frame["b"][1] = np.zeros(2)  # edit frames
    #print("frames is now: ", new_frame)
    #print("frames B is now: ", new_frame["b"])
    return tpgmm.reproduce(new_frame, np.linspace(0, 10.0, num=100))  # uses TPGMM_time to reproduce trajectory

def add_rectangles():  # add visual obstacles on plot
    rect_w, rect_y = 0.12, 0.30
    rect_1 = patches.Rectangle((-0.13 + settings.shift[0], 0.04 + settings.shift[1]), rect_w, rect_y, fill=True,
                               color='0.1')
    rect_2 = patches.Rectangle((0.11 + settings.shift[0], 0.19 + settings.shift[1]), rect_w, rect_y, fill=True,
                               color='0.1')
    rect_3 = patches.Rectangle((-0.36 + settings.shift[0], 0.04 + settings.shift[1]), 0.145, 0.45, fill=True,
                               color='0.92')
    rect_4 = patches.Rectangle((-0.36 + settings.shift[0], 0.04 + settings.shift[1]), 0.72, 0.45, fill=False,
                               color='0.1', ls='--', linewidth=0.5)
    fig, ax = plt.subplots()
    # adding boundaries
    bounds = [[[-0.1375 + settings.shift[0], 0.04 + settings.shift[1]],
               [-0.1375 + settings.shift[0], 0.34 + settings.shift[1]]],
              [[-0.13 + settings.shift[0], 0.3475 + settings.shift[1]],
               [-0.01 + settings.shift[0], 0.3475 + settings.shift[1]]],
              [[-0.0025 + settings.shift[0], 0.04 + settings.shift[1]],
               [-0.0025 + settings.shift[0], 0.34 + settings.shift[1]]],
              [[0.1025 + settings.shift[0], 0.49 + settings.shift[1]],
               [0.1025 + settings.shift[0], 0.19 + settings.shift[1]]],
              [[0.11 + settings.shift[0], 0.1825 + settings.shift[1]],
               [0.23 + settings.shift[0], 0.1825 + settings.shift[1]]],
              [[0.2375 + settings.shift[0], 0.19 + settings.shift[1]],
               [0.2375 + settings.shift[0], 0.49 + settings.shift[1]]]]
    for i in bounds:
        ax.plot([i[0][0], i[1][0]], [i[0][1], i[1][1]], '--', color='0.1', linewidth=1)
    # rect_5 = patches.Rectangle((-0.1375, 0.04), rect_w+0.015, rect_y+0.0075, fill=False, color='0.1', ls='--', linewidth=0.5)
    # rect_6 = patches.Rectangle((0.1025, 0.1825), rect_w+0.015, rect_y+0.0075, fill=False, color='0.1', ls='--', linewidth=0.5)
    # adding arcs
    eclipse_centre = [[-0.13 + settings.shift[0], 0.34 + settings.shift[1]],
                      [-0.01 + settings.shift[0], 0.34 + settings.shift[1]],
                      [0.11 + settings.shift[0], 0.19 + settings.shift[1]],
                      [0.23 + settings.shift[0], 0.19 + settings.shift[1]]]
    arc1 = patches.Arc((eclipse_centre[0][0], eclipse_centre[0][1]), 0.015, 0.015, angle=90, theta1=0, theta2=90,
                       fill=False, color='0.1', ls='--')
    arc2 = patches.Arc((eclipse_centre[1][0], eclipse_centre[1][1]), 0.015, 0.015, angle=0, theta1=0, theta2=90,
                       fill=False, color='0.1', ls='--')
    arc3 = patches.Arc((eclipse_centre[2][0], eclipse_centre[2][1]), 0.015, 0.015, angle=180, theta1=0, theta2=90,
                       fill=False, color='0.1', ls='--')
    arc4 = patches.Arc((eclipse_centre[3][0], eclipse_centre[3][1]), 0.015, 0.015, angle=-90, theta1=0, theta2=90,
                       fill=False, color='0.1', ls='--')

    ax.add_patch(rect_1), ax.add_patch(rect_2), ax.add_patch(rect_3), ax.add_patch(
        rect_4)  # , ax.add_patch(rect_5), ax.add_patch(rect_6)
    ax.add_patch(arc1), ax.add_patch(arc2), ax.add_patch(arc3), ax.add_patch(arc4)


def point_distance(pnt1, pnt2):
    return np.linalg.norm(pnt1 - pnt2)


def get_instant_jerk(s, s0, v0, a0, t):
    # s = s0 + v0*t + 1/2*a0*t^2 + 1/6*j*t^3
    v = (s - s0) / t
    a = (v - v0) / t
    jerk = 6 * (abs(s - s0) + abs(v0) * t + 1 / 2 * abs(a0) * t * t) / (t * t * t)

    return jerk, v, a


def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    angle = np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))
    vect_cross = np.cross(v1_u, v2_u)
    if vect_cross < 0:
        return angle
    else:
        return -angle


def is_in_sector(point, base_vect, centre, radius):
    pnt_vector = point - centre
    magnitude = np.linalg.norm(pnt_vector)
    angle = angle_between(pnt_vector, base_vect)
    if magnitude < radius and angle < 90 and angle > 0:
        return True
    else:
        return False


def trajectory_grid(increment, end_goal_radius):  # generate grid of trajectories
    print("generating trajectories ...")
    x_points = np.linspace(-0.36 + increment / 2 + settings.shift[0], -0.215 - increment / 2 + settings.shift[0], round(
        ((0.36 - 0.225 - increment / 2) - (0.215 - 0.225 + increment / 2)) / increment))  # x-grid generated
    y_points = np.linspace(0.04 + increment / 2 + settings.shift[1], 0.49 - increment / 2 + settings.shift[1], round(
        ((0.49 - 0.05 - increment / 2) - (0.04 - 0.05 + increment / 2)) / increment))  # y-grid generated
    print("total:", len(x_points) * len(y_points))
    point_dict = {}  # Structure: (x,y):[valid?,start_dis,end_dis,collision_states,drift,out_of_bound_states,uncertainty]
    path_array = []  # Contains all the new_path trajectories generated - split up generating and plotting functions

    jerk = [0, 0]  # changed need to be merged
    pnt_dis = 0
    v0 = [0, 0]
    a0 = [0, 0]

    for x in x_points:  # nested for loop, to cover all the grid
        for y in y_points:
            '''
            if len(settings.demos_to_use) > 2:
                new_path = p_controller(x, y)
                settings.beta = 0.1  # endpoint
                settings.gamma = 0.849  # collision
                settings.delta = 0.001  # drift
                settings.epsilon = 0.05  # out of bound
            else:
                new_path = p_controller(x, y)
                settings.beta = 0.2  # endpoint
                settings.gamma = 0.4  # collision
                settings.delta = 0.05  # drift
                settings.epsilon = 0.35  # out of bound
            '''
            new_path, MuGMR, SigmaGMR = p_controller(x, y)
            path_array.append(new_path)
            col = "green"  # assume trajectory is green = valid

            start_dis, end_dis = start_end_dis(new_path, x, y)
            if start_dis > increment / 2 or end_dis > end_goal_radius:  # if generated start point is outside radius, it is invalid
                col = "red"

            collision_states = 0
            out_of_bound_states = 0
            drift = [0, 0]  # x_drift, y_drift
            arc_centre = [[-0.13 + settings.shift[0], 0.34 + settings.shift[1]],
                          [-0.01 + settings.shift[0], 0.34 + settings.shift[1]],
                          [0.11 + settings.shift[0], 0.19 + settings.shift[1]],
                          [0.23 + settings.shift[0], 0.19 + settings.shift[1]]]

            for i in range(0, 100):
                x_val = new_path['x'][0, 0, i]
                y_val = new_path['x'][0, 1, i]
                if i >= 1:
                    jerk_instantx, v0[0], a0[0] = get_instant_jerk(x_val, new_path['x'][0, 0, i - 1], v0[0], a0[0],
                                                                   10 / 100)
                    jerk_instanty, v0[1], a0[1] = get_instant_jerk(y_val, new_path['x'][0, 1, i - 1], v0[1], a0[1],
                                                                   10 / 100)
                    jerk[0] += jerk_instantx
                    jerk[1] += jerk_instanty
                    pnt_dis += point_distance(np.array([x_val, y_val]),
                                              np.array([new_path['x'][0, 0, i - 1], new_path['x'][0, 1, i - 1]]))

                if (not (-0.36 + settings.shift[0] <= new_path['x'][0, 0, i] <= 0.36 + settings.shift[0])) or (
                not (0.04 + settings.shift[1] <= new_path['x'][0, 1, i] <= 0.49 + settings.shift[1])):  # not in bounds
                    col = "red"
                    out_of_bound_states += 1
                elif ((-0.1375 + settings.shift[0] <= x_val <= -0.0025 + settings.shift[0]) and (
                        0.04 + settings.shift[1] <= y_val <= 0.34 + settings.shift[1])) or (
                        (-0.13 + settings.shift[0] <= x_val <= -0.01 + settings.shift[0]) and (
                        0.04 + settings.shift[1] <= y_val <= 0.3475 + settings.shift[
                    1])):  # not in first rectangle and parts of the padding
                    col = "red"
                    collision_states += 1
                elif ((0.1025 + settings.shift[0] <= x_val <= 0.2375 + settings.shift[0]) and (
                        0.19 + settings.shift[1] <= y_val <= 0.49 + settings.shift[1])) or (
                        (0.11 + settings.shift[0] <= x_val <= 0.23 + settings.shift[0]) and (
                        0.1825 + settings.shift[1] <= y_val <= 0.49 + settings.shift[
                    1])):  # not in second rectangle and parts of the padding
                    col = "red"
                    collision_states += 1
                elif is_in_sector(np.array([x_val, y_val]), np.array([0, 1]), np.array(arc_centre[0]),
                                  0.0075) or is_in_sector(np.array([x_val, y_val]), np.array([1, 0]),
                                                          np.array(arc_centre[1]), 0.0075) or is_in_sector(
                        np.array([x_val, y_val]), np.array([-1, 0]), np.array(arc_centre[2]), 0.0075) or is_in_sector(
                        np.array([x_val, y_val]), np.array([0, -1]), np.array(arc_centre[3]), 0.0075):
                    col = "red"
                    collision_states += 1
                # Calculate drift score
                if i >= 1:
                    dx = (settings.goal_x - new_path['x'][0, 0, i]) - (settings.goal_x - new_path['x'][0, 0, i - 1])
                    dy = (settings.goal_y - new_path['x'][0, 1, i]) - (settings.goal_y - new_path['x'][0, 1, i - 1])
                    if dx > 0:  # this condition means the trajectory is straying away from end point in x
                        drift[0] += dx
                    if dy > 0:  # this condition means the trajectory is straying away from end point in y
                        drift[1] += dy

            point_dict[(x, y)] = [col, start_dis, end_dis, collision_states, drift, out_of_bound_states]

    point_dict = normalize(point_dict)

    return (x_points, y_points, point_dict, path_array, jerk, pnt_dis)


def start_end_dis(new_path, x, y):
    start_dis = sqrt((x - new_path['x'][0, 0, 0]) ** 2 + (
                y - new_path['x'][0, 1, 0]) ** 2)  # pythagoras to determine distance away from intended start
    end_dis = sqrt((settings.goal_x - new_path['x'][0, 0, -1]) ** 2 + (
                settings.goal_y - new_path['x'][0, 1, -1]) ** 2)  # pythag to determine distance away from goal
    return (start_dis, end_dis)


def p_controller(x_target,y_target):
    x = x_target
    y = y_target
    k_p = settings.k_p
    x_error = 10000 # initialize x_diff and y_diff to be large values
    y_error = 10000

    while abs(x_error) > settings.pd_accuracy or abs(y_error) > settings.pd_accuracy:
        new_path,MuGMR, SigmaGMR = generate_trajectory(x_init = x, y_init = y, dims = 2)

        x_error = x_target-new_path['x'][0,0,0]
        y_error = y_target-new_path['x'][0,1,0]

        x += x_error*k_p
        y += y_error*k_p
    return new_path,MuGMR, SigmaGMR


def normalize(point_dict):
    max_end_dis = max(point_dict.items(), key=lambda item: item[1][2])[1][2]
    max_drift_x = max(point_dict.items(), key=lambda item: item[1][4][0])[1][4][0]
    min_end_dis = min(point_dict.items(), key=lambda item: item[1][2])[1][2]
    min_drift_x = min(point_dict.items(), key=lambda item: item[1][4][0])[1][4][0]

    min_collision = min(point_dict.items(), key=lambda item: item[1][3])[1][3]
    max_collision = max(point_dict.items(), key=lambda item: item[1][3])[1][3]
    min_outbound = min(point_dict.items(), key=lambda item: item[1][5])[1][5]
    max_outbound = max(point_dict.items(), key=lambda item: item[1][5])[1][5]

    tot_uncertainty = 0
    # normalize each parameter
    for k in point_dict.keys():
        #(-0.57+0.455)^2+(0.425-0.005)^2+0.015
        #point_dict[k][2] = (point_dict[k][2] - min_end_dis) / (max_end_dis - min_end_dis)

        if abs(point_dict[k][2]) <= 0.015:
            point_dict[k][2] = 0
        else:
            point_dict[k][2] /= 0.45 ##sqrt((-0.57+0.455)^2+(0.425-0.005)^2)+0.015
            #point_dict[k][2] = (point_dict[k][2] - min_end_dis) / (max_end_dis - min_end_dis)
        '''
        if max_collision > 0:
            point_dict[k][3] = (point_dict[k][3] - min_collision) / (max_collision-min_collision)
        '''
        point_dict[k][3]/=30
        #point_dict[k][3] /= (100*settings.obstacle_workspace_ratio)
        if max_drift_x > 0:
            point_dict[k][4][0] = (point_dict[k][4][0] - min_drift_x) / (max_drift_x - min_drift_x)
        '''
        if max_outbound > 0:
            point_dict[k][5] = (point_dict[k][5] - min_outbound) / (max_outbound - min_outbound)
        '''
        point_dict[k][5] /=100
        #point_dict[k][5] /= 100
        uncertainty = settings.beta*point_dict[k][2]+settings.gamma*point_dict[k][3]+settings.delta*point_dict[k][4][0]+settings.epsilon*point_dict[k][5]
        point_dict[k].append(uncertainty)
        tot_uncertainty += uncertainty
    '''
    # normalize to all add to 1
    test = 0
    for k in point_dict.keys():
        point_dict[k][-1] /= tot_uncertainty
        test += point_dict[k][-1]
    print(test)
    '''
    for k in point_dict.keys():
        # prevent log 0
        if (point_dict[k][-1] > 0):
            point_dict[k][-1] = -1 * point_dict[k][-1] * log(point_dict[k][-1])

    return (point_dict)


def generate_dataframe(x_points, y_points, point_dict):
    # initialize an empty dataframe
    init_df = np.zeros((len(y_points), len(x_points)))

    df = pd.DataFrame(data=init_df, index=y_points, columns=x_points)
    for y in y_points:
        for x in x_points:
            df.at[y, x] = point_dict[(x, y)][-1]  # /point_dict[max_u_pt][5]
    return df


def check_efficacy(point_dict):  # check efficacy of task space, can calculate efficacy with this
    pts_learned = 0
    for key in point_dict:
        if point_dict[key][0] == 'green':
            pts_learned += 1
    return (pts_learned / len(point_dict))

def get_teaching_efficiency(efficacy, num_demo):
    return efficacy/num_demo

def choose_next_pt(point_dict):  # handles choosing next point for either of the rules
    if settings.rule == 0:
        pt = choose_4_cm(point_dict)
    elif settings.rule == 1:
        pt = choose_uncertainty(point_dict)

    settings.demos_to_use.append(settings.demo_name + str(int(pt)) + ".npy")  # append highest uncertainty suggestion
    return pt


def choose_4_cm(point_dict):  # handles choosing the next point for 4 cm rule
    eligible_points = []
    check_green = False
    for pt in list(point_dict.keys()):
        if point_dict[pt][0] == "green":
            check_green = True
    if len(settings.demos_to_use) == 1 or check_green == False:
        for pt in list(point_dict.keys()):  # hacky solution b/c we can't extrapolate pt # -> row and col
            col = (pt[0] - (-0.36 + settings.shift[0] + settings.increment / 2)) / x_increment
            row = (pt[1] - (0.04 + settings.shift[1] + settings.increment / 2)) / y_increment
            print(row * 4 + col)
            if settings.demo_name + str(int(round(row * 4 + col))) + ".npy" in settings.demos_to_use:
                eligible_points = append_eligible_pts(eligible_points, pt)
                break
    else:
        for pt in list(point_dict.keys()):
            if point_dict[pt][0] == "green":
                eligible_points = append_eligible_pts(eligible_points, pt)
    final_eligible_points = []

    print(eligible_points)
    eligible_points = set(eligible_points)  # elim repeats

    for pt in eligible_points:  # ensure all points added are in the red zone
        if pt in point_dict.keys():  # elim out of bounds pts, may need to delete this line b/c of rounding
            if point_dict[pt][0] == "red":
                col = (pt[0] - (-0.36 + settings.shift[0] + settings.increment / 2)) / x_increment
                row = (pt[1] - (0.04 + settings.shift[1] + settings.increment / 2)) / y_increment
                final_eligible_points.append(int(round(row * 4 + col)))
    print(final_eligible_points)

    for demo in settings.demos_to_use:  # remove pts that are already used as demos
        if demo[0:4] == 'temp':
            if int(demo[9:-4]) in final_eligible_points:
                final_eligible_points.remove(int(demo[9:-4]))
        else:
            if int(demo[4:-4]) in final_eligible_points:
                final_eligible_points.remove(int(demo[4:-4]))

    final_pt = random.choice(final_eligible_points)
    return final_pt


def append_eligible_pts(eligible_points, pt):  # brute force method of identifying adjacent points
    eligible_points.append((pt[0] + x_increment, pt[1]))
    eligible_points.append((pt[0] - x_increment, pt[1]))
    eligible_points.append((pt[0] + x_increment, pt[1] - y_increment))
    eligible_points.append((pt[0] - x_increment, pt[1] - y_increment))
    eligible_points.append((pt[0] + x_increment, pt[1] + y_increment))
    eligible_points.append((pt[0] - x_increment, pt[1] + y_increment))
    eligible_points.append((pt[0], pt[1] + y_increment))
    eligible_points.append((pt[0], pt[1] - y_increment))
    return eligible_points


def choose_uncertainty(point_dict):  # handles choosing the next point for uncertainty
    sorted_dict = {k: v for k, v in sorted(point_dict.items(),
                                           key=lambda item: item[1][-1])}  # Sort dictionary by increasing uncertainty

    # elims existing starting points from possible next points
    for pt in list(sorted_dict.keys())[::-1]:
        col = (pt[0] - (-0.36 + settings.shift[0] + settings.increment / 2)) / x_increment
        row = (pt[1] - (0.04 + settings.shift[1] + settings.increment / 2)) / y_increment
        if settings.demo_name + str(int(round(row * 4 + col))) + ".npy" not in settings.demos_to_use:
            max_uncertainty_pt = pt
            break
    col = (max_uncertainty_pt[0] - (-0.36 + settings.shift[0] + settings.increment / 2)) / x_increment
    row = (max_uncertainty_pt[1] - (0.04 + settings.shift[1] + settings.increment / 2)) / y_increment

    print("Point with highest uncertainty:", max_uncertainty_pt, "Uncertainty:",
          sorted_dict[list(sorted_dict.keys())[-1]])
    return int(round(row * 4 + col))


def plot(path_array, point_dict, max_u_pt, pt_num):
    plot1 = plt.figure(1)
    add_rectangles()
    plot_demos()  # visually plot demos extracted
    plt.axis('scaled')  # ensure 1:1 scaling of the plot
    index_ref = list(point_dict)
    for i in range(len(path_array)):
        col = point_dict[index_ref[i]][0]
        x = index_ref[i][0]
        y = index_ref[i][1]
        plt.plot(path_array[i]['x'][0, 0, :], path_array[i]['x'][0, 1, :], color=col,
                 linewidth=0.3)  # plot generated trajectory
        plt.plot(x, y, marker="+", color=col, markersize=5, linewidth=0.3)  # visual marker
        plt.gca().add_artist(plt.Circle((x, y), settings.increment / 2, color=col, fill=False,
                                        linewidth=0.3))  # visual marker of acceptable radius
    # plt.plot(max_u_pt[0],max_u_pt[1],marker=".",color="blue",markersize=5,linewidth=0.3) #highlight max uncertainty point
    plt.gca().add_artist(
        plt.Circle((settings.goal_x, settings.goal_y), settings.end_goal_radius, color="0.1", fill=False,
                   linewidth=0.3))  # visual marker of acceptable radius

    plt.gca().invert_xaxis()
    plt.gca().invert_yaxis()
    plt.savefig(str(pt_num) + ".png", dpi=900)
    plt.clf()

    plot2 = plt.figure(2)
    plt.axis('scaled')
    ax = sns.heatmap(df, cmap="coolwarm")
    ax.invert_yaxis()
    ax.invert_xaxis()
    plt.savefig(str(pt_num) + "heatmap.png", dpi=900)
    plt.clf()

def initialize_csv():
    timestr = time.strftime("%Y%m%d-%H%M%S")
    #current_date_and_time_string = str(current_date_and_time)
    filename = "Experiement_1_result_" + timestr + ".csv"
    csv_file = open(filename, 'w')
    writer = csv.writer(csv_file)
    if settings.rule == 1:
        header = ['Starting point', 'teaching efficiency entropy', 'demo used in entropy', "number of demos needed", "total jerk (x,y)", "point distance", "variance", "BIC components"]
    else:
        header = ['Starting point', 'teaching efficiency 4cm', 'demo used in 4cm', "number of demos needed",
                  "total jerk (x,y)", "point distance", "variance"]
    writer.writerow(header)
    csv_file.close()
    return writer, csv_file, filename

def get_bic_score(start, end, demos_to_use):
    bic_score = []
    for i in range(start, end+1):
        tpgmm = TPGMM_Time(i, reg_covar=settings.reg, max_steps=200)  # initialize TPGMM_Time
        data = retrieve_demos(demos_to_use)  # obtain demos
        frames = get_frames(data, dims=2)  # generate frames
        # print ("frame is: " + str(frames))
        tpgmm.fit(data, frames)  # fit data to frames
        data_bic = data[:, :, 0]
        for i in range(1, data.shape[2]):
            data_temp = data[:, :, i]
            data_bic = np.concatenate((data_bic, data_temp))
        # print("data_bic is: " + str(data_bic))

        # plot1 = plt.figure(1)

        # x_points, y_points, point_dict, max_u_pt, error_traj = trajectory_grid(settings.increment, settings.end_goal_radius)  # generate grid of trajectories
        bic_score.append(tpgmm.bic_test(data_bic, np.linspace(0, 10.0, num=100)))
    return bic_score


# --- MAIN --- #
settings = Params()

obtain_files()

#tpgmm = TPGMM_Time(10, max_steps=200)  # initialize TPGMM_Time

x_increment = abs((-0.36 + settings.increment / 2) - (-0.215 - settings.increment / 2)) / 3
y_increment = abs((0.04 + settings.increment / 2) - (0.49 - settings.increment / 2)) / 13

writer, csv_file,filename = initialize_csv()

for point in range(56):
    settings.demos_to_use.append(settings.demo_name + str(point) + ".npy")
    min_index_list = []
    for x in range(
            settings.max_demos):  # max number of demos to go through before saying generalization is not gonna happen
        for demo_name in settings.demos_to_use:
            data = np.expand_dims(np.load(settings.directory_name + '/' + demo_name), axis=2)  # examine demo 1
            print(demo_name, data[0, 1, 0], data[0, 2, 0])  # print out starting point

        print(settings.demos_to_use)
        bic_score = []
        min_index = 11
        starting_component = 1
        bic_score = get_bic_score(starting_component, settings.max_gmm, settings.demos_to_use)
        # get the minimum index
        min_index = bic_score.index(min(bic_score[0:])) + starting_component-1
        print("bic score is: ", bic_score)
        print("min bic score num of elements is: ", min_index)
        #print("demos used right now is on: ", len(demos_to_use))
        tpgmm = TPGMM_Time(min_index, reg_covar=settings.reg, max_steps=200)  # initialize TPGMM_Time
        min_index_list.append(min_index)
        data = retrieve_demos(settings.demos_to_use)  # obtain demos

        frames = get_frames(data, dims=2)  # generate frames
        priors, Mu, Sigma = tpgmm.fit(data, frames)  # fit data to frames

        x_points, y_points, point_dict, path_array, jerk, pnt_dis = trajectory_grid(settings.increment,
                                                                                    settings.end_goal_radius)  # generate grid of trajectories

        df = generate_dataframe(x_points, y_points, point_dict)
        efficacy = check_efficacy(point_dict)
        if (efficacy>= 0.9):
            efficiency = get_teaching_efficiency(efficacy,len(settings.demos_to_use))
            break
        elif x != settings.max_demos - 1:
            # Convert uncertainty selection to file number
            max_u_pt = choose_next_pt(point_dict)
    #header = ['Starting point', 'teaching efficiency entropy', 'teaching efficiency 4cm rule', 'demo used in entropy',
    #          'demo used in entropy']
    #writer.writerow(header)
    #start_point_string = str(settings.demos_to_use[0])
    #demoused = "demo" + str(settings.demos_to_use[0])
    #for i in range(1,len(settings.demos_to_use)):
    start_point_string = str(settings.demos_to_use)
    demoused = str(settings.demos_to_use)
    test_frame = [start_point_string, str(efficiency), demoused, str(len(settings.demos_to_use)), str(jerk), str(pnt_dis), str(sum(sum(sum(map(sum, Sigma))))), str(min_index_list)]
    with open(filename, 'a') as fd:
        writer = csv.writer(fd)
        writer.writerow(test_frame)
    #writer.writerow(test_frame)

    plot(path_array, point_dict, max_u_pt,
         point)  # plot the final result (either efficiency reached, or max # of demos has been reached)
    settings.demos_to_use = []  # put the next starting point in here